/**
 * Created by bertr on 10/9/2016.
 */

function getCookie(name) {
    var cookieValue = null;
    if(document.cookie && document.cookie != ''){
        var cookies = document.cookie.split(';');
        for(var i = 0; i < cookies.length; i++){
            var cookie = jQuery.trim(cookies[i]);
            if(cookie.substring(0,name.length + 1) == (name + '=')){
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue
}

function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTION|TRACE)$/).test(method)
}



function sinkTheData() {
     $('#event_form').submit(function(event) {
        event.preventDefault();
        var csrftoken = getCookie('csrftoken');
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken)
                }
            }
        });
        

        var eventId = $('#event_id').val()
        var name = $('#id_name').val();
        var tagline = $('#id_tagline').val();
        var featuredEvent = $('#id_featured_event').is(':checked')
        var picture = $('#id_pictures').val();
        var startTime = $('#id_start_time').val();
        var endTime = $('#id_end_time').val();
        var link = $('#id_url').val();
        var venue = $('#id_venue').val();



            if(name == ''){
                alert("Name must have value");
                $('#id_name').css('box-shadow', '1px 1px 25px red');
                return;
            }

            if(tagline == ''){
                alert("Tagline must have value");
                $('#id_tagline').css('box-shadow', '1px 1px 25px red');
                return;
            }

            if(Date(Date(startTime)) > Date(Date(endTime))){
                alert("Ending Date/Time must be after start date/time");
                $('#id_start_time').css('box-shadow', '1px 1px 25px red');
                $('#id_end_time').css('box-shadow', '1px 1px 25px red');
                return;
            }

            if(link == ''){
                alert("Must have a valid URL");
                 $('#id_url').css('box-shadow', '1px 1px 25px red');
                return;
            }

            var $form = $(this),
                url = $form.attr('action');
         var formData = new FormData(document.querySelector("#event_form"));
         formData.append("eventId", eventId);
         formData.append("name", name);
         formData.append("tagline", tagline);
         formData.append("featured_event", featuredEvent);
         formData.append("pictures", $('#id_pictures')[0].files[0]);
         formData.append("start_time", startTime);
         formData.append("end_time", endTime);
         formData.append("url", link);
         formData.append("venue", venue);

        $.ajax({
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                 $('body').html(data)
            }
        });

            // var posting = $.post(
            //     url,
            //     {
            //         formData,
            //     }
            //     );
            //
            //     posting.done(function (data) {
            //         $('body').html(data)
            //     })

        });
}


function dtCss() {
     $('#id_start_time').datetimepicker({
                controlType: 'select',
                oneLine: true,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'hh:mm:00tt'
                });

                $('#id_end_time').datetimepicker({
                controlType: 'select',
                oneLine: true,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'hh:mm:00tt'
                });


            $('#reset-id-reset').click(function () {
                $('#event_id').val('');
                $('#id_name').val('');
                $('#id_name').text('');
                $('#id_tagline').val('');
                $('#id_tagline').text('');
                $('#id_start_time').val('');
                $('#id_start_time').text('');
                $('#id_end_time').val('');
                 $('#id_end_time').text('');
                $('#id_url').val('');
                 $('#id_url').text('');
                $('#id_venue').val('');
                $('#id_venue').text('');
            });
}

$(document).ready(function(){
        sinkTheData();
        dtCss();
        // var isContains = $('#div_id_pictures').text().indexOf("Clear") > -1;
        // alert(isContains);
    })

function eventCall(id){
    console.log($(this));
    $.ajax({
        type: 'get',
        url: 'event_form/',
        data: {'id': id},

        success: function (data) {
            $('#eventFormDiv').html(data);
            sinkTheData();
            dtCss();
           // var isContains = $('#div_id_pictures').text().indexOf("Clear") > -1;
           //  alert(isContains);
        }
    });


}


function deleteEvent(id, name){
    confirmation = confirm("Ok to delete " + name + "?");
    if(confirmation == true){
        var csrftoken = getCookie('csrftoken');
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken)
                }
            }
        });
        $.ajax({
            type: 'post',
            url: 'delete_event/',
            data: {'id': id},
            success: function (data) {
                $('#' + id).html('');
                alert(name + " has been deleted.")
            }
        })
    }else{
        return;
    }
}

