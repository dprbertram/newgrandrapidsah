import json
from django.shortcuts import render, get_object_or_404, redirect, Http404, HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.utils.timezone import now, timedelta
from .models import Venue, Event
from .eventforms import EventForm
from datetime import datetime
from django.views.decorators.csrf import csrf_protect
from django.template.loader import render_to_string
from mailchimp3 import MailChimp


def event_index(request):
    # set the time variables
    date = now().today()
    tomorrow = date + timedelta(1)
    start_week = date - timedelta(date.weekday())
    endOfWeek = start_week + timedelta(6)
    events = Event.objects.filter(start_time__range=[now(), now() + timedelta(500)]).order_by('start_time').exclude(isDeleted=True)
    # gather the context of events
    # todays_events = Event.objects.filter(end_time__range=[now(), now() + timedelta(seconds=86399)]).order_by(
    #     'start_time').exclude(isDeleted=True)
    # eventsLeftInWeek = Event.objects.filter(end_time__range=[tomorrow, endOfWeek]).order_by('start_time').exclude(
    #     isDeleted=True)
    # theRestofTheEvents = Event.objects.filter(
    #     end_time__range=[endOfWeek + timedelta(1), endOfWeek + timedelta(100)]).order_by('start_time').exclude(
    #     isDeleted=True)
    # featuredEvents = Event.objects.filter(featured_event=True).exclude(isDeleted=True)

    isEventManager = False
    if request.user is not None:
        isEventManager = request.user.groups.filter(name='Event_Manager').exists()
        isAdmin = request.user.groups.filter(name='admin').exists()

    context = {'events': events,
               'currentPage': request.get_full_path,  # used in main nav bar partial
               'isEventManager': isEventManager,
               'isAdmin': isAdmin,
               }
    return render(request, 'events/event_index.html', context)

def event_list(request):
    redirect('event_index')


def event_manager(request):

    if request.user.is_staff:
        events = Event.objects.all().filter(start_time__gte=(now() - timedelta(hours=2))).exclude(isDeleted=True)
        eventform = EventForm
        context = {'events' : events, 'eventform': eventform }  # 'eventform': EventForm,  saved for later
        return render(request, 'event_manager.html', context)
    return redirect("/")

@csrf_protect
def event_form(request):
    if request.POST:
        tfmt1 = "%Y-%m-%d %I:%M:%S%p"
        tfmt2 = "%Y-%m-%d %H:%M:%S"
        p = request.POST
        eventname = p["name"]
        tagline = p["tagline"]
        url = p["url"]
        if p["featured_event"] == 'false':
            featured_event = False
        else:
            featured_event = True

        # 'm'  on the end of string means using 'am/pm' else using military time
        if p["start_time"][-1] == 'm':
            start = datetime.strptime(p["start_time"], tfmt1)
            end = datetime.strptime(p["end_time"], tfmt1)
        else:
            start = datetime.strptime(p["start_time"], tfmt2)
            end = datetime.strptime(p["end_time"], tfmt2)

        photo = None
        if "pictures" in request.FILES.keys():
            photo = request.FILES['pictures']
        venue = Venue.objects.get(id=int(p["venue"]))
        if p['event_id']:
            event = Event.objects.get(pk=p['event_id'])
            event.name = eventname
            if(photo != None):
                event.pictures = photo
            event.tagline = tagline
            event.venue = venue
            event.start_time = start
            event.end_time = end
            event.url = url
            event.featured_event = featured_event
            event.save()
        else:
            event = Event(name=eventname, pictures=photo, venue=venue, tagline=tagline, start_time=start, end_time=end,
                          url=url, featured_event=featured_event)
            event.save()
        return redirect('event_manager/')
    elif request.is_ajax():
        eventId = request.GET['id']
        event = get_object_or_404(Event,id=eventId)
        form = EventForm(request.POST or None, instance = event)
        context = {'event': event, 'form': form, 'eventId': eventId}
        html = render_to_string('eventmanager/event_form.html', context, request = request)
        return HttpResponse(html)

    else:
        Http404(request)


def event_users(request, username):
    user = get_object_or_404(User, username=username)
    if request.user == user:
        events = user.events.all()
    else:
        events = Event.public.filter(owner__username=username)
    context = {'events': events, 'owner': user}
    return render(request, 'events/event_user.html', context)

def signup(request):
    currentPage = request.get_full_path
    context = { 'request': request,
                'currentPage': currentPage
                }
    if request.method == 'GET':
        return render(request, 'events/signup.html', context)
    elif request.method == 'POST':
        p = request.POST
        if p['weeklybeat']:
            client = MailChimp('ArenaDistrictGrandRapids', '4feee3b4963d412b5b696943e0788ff3-us10')
            client.member.create(
                '7cb70e985b',
                {
                    'email_address': p['email'],
                    'status':'subscribed',
                    'merge_fields':{
                        'FNAME': p['firstname'],
                        'LNAME': p['lastname'],
                    },
                }
            )
        if p['password'] == p['password_confirmation']:
             newUser = User.objects.create_user(p['username'], email=p['email'], password=p['password'])
             newUser.first_name = p['firstname']
             newUser.last_name  = p['lastname']
             newUser.set_password(p['password'])
             newUser.save()
             login(request, newUser)
        return redirect('/')

def subscribe(request):
    currentPage = request.get_full_path
    context = {'request': request,
               'currentPage': currentPage
               }
    if request.method == 'GET':
        return render(request, 'events/subscribe.html', context)
    elif request.method == 'POST':
        p = request.POST
        client = MailChimp('ArenaDistrictGrandRapids', '4feee3b4963d412b5b696943e0788ff3-us10')
        client.member.create(
            '7cb70e985b',
            {
                'email_address': p['email'],
                'status': 'subscribed',
                'merge_fields': {
                    'FNAME': p['firstname'],
                    'LNAME': p['lastname'],
                },
            }
        )

        return redirect('/')

def weeklybeat(request):
    date = now().today()
    start_week = date - timedelta(date.weekday())
    daysOfWeek = []

    featuredEvents = Event.objects.filter(featured_event=True)

    mondayDate = start_week
    daysOfWeek.append(mondayDate)
    monday = Event.objects.filter(end_time__range=[mondayDate,
                                                   start_week + timedelta(seconds=86399)])
    tuesdayDate = start_week + timedelta(1)
    tuesday = Event.objects.filter(end_time__range=[tuesdayDate,
                                                tuesdayDate + timedelta(seconds=86399)])
    daysOfWeek.append(tuesdayDate)
    wednesdayDate = start_week + timedelta(2)
    wednesday = Event.objects.filter(end_time__range=[wednesdayDate,
                                                    wednesdayDate + timedelta(seconds=86399)])
    daysOfWeek.append(wednesdayDate)

    thursdayDate = start_week + timedelta(3)
    thursday = Event.objects.filter(end_time__range=[thursdayDate,
                                                     thursdayDate + timedelta(seconds=86399)])
    daysOfWeek.append(thursdayDate)




    context = {
        'featured':featuredEvents,
        'monday': monday,
        'tuesday': tuesday,
        'wednesday': wednesday,
        'thursday': thursday,
        'weekdays': daysOfWeek,
    }
    return render(request, 'weeklybeat/weeklybeat_list.html', context)


def weekendbeat(request):
    date = now().today()
    start_week = date - timedelta(date.weekday())
    daysOfWeek = []

    featuredEvents = Event.objects.filter(featured_event=True)

    fridayDate = start_week + timedelta(4)
    friday = Event.objects.filter(end_time__range=[fridayDate,
                                                   fridayDate + timedelta(seconds=86399)])
    daysOfWeek.append(fridayDate)

    saturdayDate = start_week + timedelta(5)
    saturday = Event.objects.filter(end_time__range=[saturdayDate,
                                                    saturdayDate + timedelta(seconds=86399)])
    daysOfWeek.append(saturdayDate)

    sundayDate = start_week + timedelta(6)
    sunday = Event.objects.filter(end_time__range=[sundayDate,
                                                   sundayDate + timedelta(seconds=86399)])

    context = {
        'featured': featuredEvents,
        'friday': friday,
        'saturday': saturday,
        'sunday': sunday,
        'weekdays': daysOfWeek,
    }
    return render(request, 'weeklybeat/weekendbeat.html', context)


def deleteEvent(request):
    if request.is_ajax():
        event = Event.objects.get(pk=request.POST['id'])
        event.isDeleted = True
        event.save()
        return render(request, 'managed_event.html')