from django.utils import timezone
from django import template


register = template.Library()

@register.filter
def right_now(time):
    return time > timezone.now()