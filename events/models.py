import this

from django.contrib.auth.models import User, Group
from django.db import models
from django.utils.timezone import datetime, now
from datetime import timedelta


class AllFutureEvents(models.Manager):
    def get_queryset(self):
        qs = super(AllFutureEvents, self).get_queryset()
        return qs.filter(isDeleted=False).filter(end_time__gte=now())


class Venue_Type(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        verbose_name = 'venue type'
        verbose_name_plural = 'venue types'
        ordering = ['name']

    def __str__(self):
        return self.name

class Event_Type(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        verbose_name = 'event type'
        verbose_name_plural = 'event types'
        ordering = ['name']

    def __str__(self):
        return self.name

class AuthorizedUsersGroup(models.Model):
    name = models.CharField('name', max_length=60, blank=False)
    user = models.ManyToManyField(User, blank=False)

    def __str__(self):
        return self.name



class Venue(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=255, blank=False)
    description = models.TextField('description', blank=True)
    address = models.CharField('street address', max_length=255, blank=False)
    suite = models.CharField('suite number', max_length=100, blank=True)
    venue_type = models.ManyToManyField(Venue_Type, blank=True)
    url = models.URLField(blank=True)
    facebook = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    logo = models.ImageField('logo', blank=True)
    Managers = models.ForeignKey(AuthorizedUsersGroup, blank=True, null=True)

    class Meta:
        verbose_name = 'venue'
        verbose_name_plural = 'venues'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Venue, self).save(*args, **kwargs)

# class Event_Photos(models.Model):
#     name = models.CharField('name', max_length=50, blank=False, default="picture")
#     pictures = models.ImageField(upload_to='media/images/%Y/%m/%d')
#     primary = models.BooleanField('primary', default=False)
#
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         verbose_name = 'picture'
#         verbose_name_plural = 'pictures'
#



class Event(models.Model):
    name = models.CharField('name', max_length=100, blank=False)
    pictures = models.ImageField(upload_to='media/images/%Y/%m/%d', blank=True)
    tagline = models.CharField('tagline', max_length=200, blank=True)
    venue = models.ForeignKey(Venue)
    description = models.TextField('description', blank=True)
    url = models.URLField('Event Link', blank=True)
    facebook = models.URLField("Facebook Link", blank=True)
    twitter = models.URLField("Twitter Link", blank=True)
    is_public = models.BooleanField('public', default=True)
    vip_group = models.ManyToManyField(Group, 'Group', blank=True)
    start_time = models.DateTimeField('start time', blank=True)
    end_time = models.DateTimeField('end time', blank=True)
    date_created = models.DateTimeField('date created')
    date_updated = models.DateTimeField('date updated')
    isDeleted = models.BooleanField('isDeleted', default=False)
    featured_event = models.BooleanField('featured event', default=False)


    objects = models.Manager()
    public = AllFutureEvents()

    class Meta:
        verbose_name = 'event'
        verbose_name_plural = 'events'
        ordering = ['-start_time']

    def __str__(self):
        return self.name

    def primary_photo(self):
        return self.pictures.filter(primary=True)


    def save(self, *args, **kwargs):
        if not self.id:
            self.date_created = now()
        self.date_updated = now()
        super(Event, self).save(*args, **kwargs)



