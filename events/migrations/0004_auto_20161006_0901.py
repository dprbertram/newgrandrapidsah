# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-10-06 13:01
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20161006_0901'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 6, 12, 1, 46, 437250), verbose_name='end time'),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 6, 9, 1, 46, 437219), verbose_name='start time'),
        ),
    ]
