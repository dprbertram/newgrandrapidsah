# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-10-02 20:18
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='end_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 2, 19, 18, 16, 927577), verbose_name='end time'),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_time',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 2, 16, 18, 16, 927577), verbose_name='start time'),
        ),
        migrations.AlterField(
            model_name='venue',
            name='name',
            field=models.CharField(max_length=255, verbose_name='name'),
        ),
    ]
