from django import forms
from .models import Venue_Type


class VenueForm(forms.Form):
    venue_id = forms.IntegerField(
        type="hidden"
    )

    venue_name = forms.CharField(
        label="Venue Name",
        max_length=150,
    )

    venue_address = forms.CharField(
        label="Venue Address",
        max_length=200,
    )

    venue_type = forms.ModelChoiceField(
        queryset=Venue_Type.objects.all(),
        to_field_name="name",
    )

    venue_url = forms.CharField(
        label="Web Site Link",
        max_length=200,
    )