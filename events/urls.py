from django.conf.urls import url
from django.core.urlresolvers import reverse_lazy
from grandrapidsafterhours import settings

app_name = 'events'
urlpatterns = [
    url(r'^user/(?P<username>[-\w]+)/$', 'events.views.event_users',
        name='events_event_user'),
    url(r'^$', 'events.views.event_index', name="event_index" ),
    url(r'^event_list/$', 'events.views.event_index', name='event_list'),
    url(r'event_manager/$', 'events.views.event_manager', name='event_manager'),
    url(r'event_manager/event_form/$', 'events.views.event_form', name='event_form'),
    url(r'signup', 'events.views.signup', name='signup'),
    url(r'^weeklybeat/$', 'events.views.weeklybeat', name='weeklybeat'),
    url(r'^weekendbeat/$', 'events.views.weekendbeat', name='weekendbeat'),
    url(r'/delete_event/$', 'events.views.deleteEvent', name='deleteEvent'),
    url(r'subscribe', 'events.views.subscribe', name='subscribe'),
]
if settings.DEBUG:
  urlpatterns.append(url(r'^media/(?P<path>.*)$', 'django.views.static.serve', { 
        'document_root': settings.MEDIA_ROOT,
      }),)

