from django.contrib import admin

from .models import Event, Venue, Venue_Type, Event_Type, AuthorizedUsersGroup


class EventAdmin(admin.ModelAdmin):
    list_display = ('name',  'tagline', 'description', 'url', 'is_public',  'start_time', 'end_time')
    list_editable = ('name', 'tagline', 'description', 'url', 'is_public', 'start_time', 'end_time')
    search_fields = ['url', 'description']
    readonly_fields = ('date_created', 'date_updated')


admin.site.register(Venue)
admin.site.register(Venue_Type)
admin.site.register(Event, EventAdmin)
admin.site.register(Event_Type)
admin.site.register(AuthorizedUsersGroup)



