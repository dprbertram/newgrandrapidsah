from django.contrib.auth.models import User

from django import forms


class SignupForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'firstname', 'lastname', 'email')

    

    def __unicode__(self):
        return self.username + "(" + self.email +")"