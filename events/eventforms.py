from django import forms
from django.contrib.admin import widgets
from .models import Venue, Event
from django.conf.urls import url
from crispy_forms.helper import FormHelper
from django.core.urlresolvers import reverse_lazy, reverse
from crispy_forms.layout import Layout, MultiField, Submit, Div, ButtonHolder, Field, HTML, Reset
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['id', 'name', 'pictures', 'tagline', 'venue', 'url', 'start_time', 'end_time', 'featured_event']


    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args,**kwargs)
        form_method = 'post'
        form_id = 'event-form-id'
        kwargs = super(Event)
        form_show_errors = True
        render_hidden_fields = True
        self.helper = FormHelper(self)
        self.helper.form_id = 'event_form'
        self.helper.form_action = 'event_form/'
        self.helper.layout = Layout(
            Div(
                Div('name', css_class="col-md-4"),
                Div('tagline', css_class="col-md-6"),
                Div('featured_event', css_class="col-md-2 checkbox", css_id="featured_event_check"),
                css_class='row'
                ),
            Div(
                Div('pictures', css_class="col-md-4"),
                Div('url', css_class="col-md-8"),
                css_class='row'
            ),
            Div('venue', css_class="col-md-6"),
            Div(
                Div('start_time', css_class="col-md-4"),
                Div('end_time', css_class="col-md-4"),
                css_class='row col-md-offset-3'
            ),
            Div(
                FormActions(
                    Submit('submit', 'Submit', css_class="btn-primary"),
                    Reset('reset', "Reset")

                ),


                css_class='row col-md-offset-0'
            ),

        HTML(
            """
            <input  type="hidden" value="{{ event.id }}" id="event_id" name="event_id">

            """
        )

        )








